# Entity Extractor
An API to parse and extract department and university entities from text.

This is a basic wrapper around two other APIs - an LLM and the ROR matcher. It amalgamates the results of those API calls and returns them in a single record.

![license](https://img.shields.io/gitlab/license/crossref/labs/entext) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/entext)

![ChatGPT](https://img.shields.io/badge/chatGPT-74aa9c?style=for-the-badge&logo=openai&logoColor=white)
 ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![ROR](https://img.shields.io/badge/ror-F88900?style=for-the-badge&logoColor=white)

## Installation

    pip install entext

## CLI Usage

Place your OpenAI API key in a file called `.openai` in your home directory. Then run:

    python3 -m entext.entext --help

    Usage: entext.py [OPTIONS] AFFILIATION

    Resolve an affiliation string into a department and ROR ID

    ╭─ Arguments ─────────────────────────────────────────────╮
    │ * affiliation         TEXT  [default: None] [required]  │
    ╰─────────────────────────────────────────────────────────╯
    ╭─ Options ───────────────────────────────────────────────╮
    │ --help                Show this message and exit.       │
    ╰─────────────────────────────────────────────────────────╯

## Programmatic Usage
    from entext import entext

    affiliation = "Department of Ceramics, Brown University"

    print(
        entext.Resolver(
            llm_callable=entext.open_ai.OpenAI.parse,
            ror_callable=entext.ror.RORMatcher.match,
        ).resolve(affiliation)
    )

## Example Response
Command:

    python3 -m entext.entext "Department of Literature, Birkbeck, University of London, UNITED KINGDOM"

Yields:

    {
        'llm': {'department': 'Department of Literature', 'university': 'Birkbeck, University of London', 'country': 'GB'},
        'ror': {
            'substring': 'Birkbeck, University of London',
            'score': 1.0,
            'matching_type': 'EXACT',
            'chosen': True,
            'organization': {
                'id': 'https://ror.org/02mb95055',
                'name': 'Birkbeck, University of London',
                'email_address': None,
                'ip_addresses': [],
                'established': 1823,
                'types': ['Education'],
                'relationships': [{'label': 'University of London', 'type': 'Parent', 'id': 'https://ror.org/04cw6st05'}, {'label': 'Institute of Structural and Molecular Biology', 'type': 'Child', 'id': 'https://ror.org/05wsetc54'}],
                'addresses': [
                    {
                        'lat': 51.50853,
                        'lng': -0.12574,
                        'state': None,
                        'state_code': None,
                        'city': 'London',
                        'geonames_city': {
                            'id': 2643743,
                            'city': 'London',
                            'geonames_admin1': {'name': 'England', 'id': 6269131, 'ascii_name': 'England', 'code': 'GB.ENG'},
                            'geonames_admin2': {'name': 'Greater London', 'id': 2648110, 'ascii_name': 'Greater London', 'code': 'GB.ENG.GLA'},
                            'license': {'attribution': 'Data from geonames.org under a CC-BY 3.0 license', 'license': 'http://creativecommons.org/licenses/by/3.0/'},
                            'nuts_level1': {'name': None, 'code': None},
                            'nuts_level2': {'name': None, 'code': None},
                            'nuts_level3': {'name': None, 'code': None}
                        },
                        'postcode': None,
                        'primary': False,
                        'line': None,
                        'country_geonames_id': 2635167
                    }
                ],
                'links': ['http://www.bbk.ac.uk/front-page'],
                'aliases': ['Birkbeck College'],
                'acronyms': ['BBK'],
                'status': 'active',
                'wikipedia_url': 'http://en.wikipedia.org/wiki/Birkbeck,_University_of_London',
                'labels': [{'label': 'Birkbeck, Prifysgol Llundain', 'iso639': 'cy'}],
                'country': {'country_name': 'United Kingdom', 'country_code': 'GB'},
                'external_ids': {
                    'ISNI': {'preferred': None, 'all': ['0000 0001 2324 0507']},
                    'FundRef': {'preferred': '501100021082', 'all': ['501100021082']},
                    'HESA': {'preferred': None, 'all': ['0127']},
                    'UCAS': {'preferred': None, 'all': ['B24']},
                    'UKPRN': {'preferred': None, 'all': ['10007760']},
                    'OrgRef': {'preferred': None, 'all': ['704022']},
                    'Wikidata': {'preferred': None, 'all': ['Q375606']},
                    'GRID': {'preferred': 'grid.88379.3d', 'all': 'grid.88379.3d'}
                }
            }
        }
    }


## Credits
&copy; Crossref 2023. Code by Martin Paul Eve.